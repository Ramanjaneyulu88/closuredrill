const limitFunctionCallCount = require("../limitFunctionCallCount")


let callback = (n) => console.log(n);
  
let limitFunc = limitFunctionCallCount(callback, 3);

limitFunc();
limitFunc();
limitFunc();
limitFunc();