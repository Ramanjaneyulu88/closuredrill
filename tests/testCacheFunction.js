const cacheFunction = require("../cacheFunction")

let callback1 = (n) => n
let invoker = cacheFunction(callback1)

console.log(invoker(1))
console.log(invoker(2))
console.log(invoker(1))