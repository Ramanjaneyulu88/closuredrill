function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
  
    let i = 0;
  
    return function () {
      if (i < n) {
        cb(i);
        i += 1;
      } else {
        console.log(null);
      }
    };
  }
  
 module.exports = limitFunctionCallCount